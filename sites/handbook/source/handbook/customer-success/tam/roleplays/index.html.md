---
layout: handbook-page-toc
title: "Roleplay Scenarios"
description: "This handbook page collects links to all roleplaying scenarios, for TAMs to utilize to improve their conversations and enable them to be audible-ready."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Roleplay Scenarios

- [Success planning](https://about.gitlab.com/handbook/customer-success/tam/success-plans/) and uncovering strategic goals
   - "Ticklish Licorice" scenario
      - [Customer info sheet](https://docs.google.com/document/d/19YWLSP6Kol5WwP83I01CSnbCwvdMUa6LXsiihOTNItI/edit#heading=h.qiwberg2gd4s)
      - [TAM info sheet](https://docs.google.com/document/d/1Jmkmz3cw4sFQdwfAw-hMd4HnhIjxW5pEumKstq0laiI/edit)
      - This scenario can be done 1:1 where the person acting as the customer can choose to be George or Alicia, but this scenario can be extra impactful in a group setting with multiple people acting as different customer personas.
   - "Green Grass Medical" scenario
      - [Background info](https://gitlab.edcast.com/pathways/tam-building-success-plans/cards/922704)
      - This roleplay is part of the "TAM Building Success Plans" EdCast course
- [Stage expansion](https://about.gitlab.com/handbook/customer-success/tam/stage-enablement-and-expansion/)
   - TODO build scenario and roles
- Booking an [EBR](https://about.gitlab.com/handbook/customer-success/tam/ebr/)
- [Onboarding](https://about.gitlab.com/handbook/customer-success/tam/onboarding/)/customer kickoffs
   - Kickoff
      - [Rubric](https://docs.google.com/forms/d/e/1FAIpQLSeZgqf6cU0rR0wvoOneGGh0jNaC0PXCzN5TEf_IBbBn80VxfQ/viewform)
          - Part of [Sales Quick Start](https://about.gitlab.com/handbook/sales/onboarding/)
      - TODO build scenario and roles
- Pre-sales
   - [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/)
      - Part of [Sales Quick Start](https://about.gitlab.com/handbook/sales/onboarding/)
      - [Buyer info sheet](https://docs.google.com/document/d/1Zuy4z2YHZR0GXdQB_zexiknDKllgRab59wFWj3kpVnU/edit)
      - [Seller info sheet](https://docs.google.com/document/d/1jwLo3GYA81VNcXg7vHTRF7iMkF7YihV7a362yPtZx0o/edit)

### Customer Personas

These role-play scenarios are designed to practice identifying and gaining access to people in the listed [personas](/handbook/customer-success/tam/engagement#customer-personas).

#### Development Lead or Other Stakeholder

The defined persona for [somebody in a leadership role within the customer's development or engineering department](/handbook/customer-success/tam/engagement#development-lead).

- [TAM role](https://docs.google.com/document/d/14UFM4x6Q1QWymydyTWryokHhEXlOvqjyGEhqtXa6ZDA/edit?usp=sharing)
- [Customer role](https://docs.google.com/document/d/1o8IjSdwLe1ZVfI4bsUYrRdDgXGs7e56vz2vhJAYpI2A/edit?usp=sharing)
- [Post-scenario review](https://docs.google.com/document/d/1kzRyNXx-HyZxrjNY-pxCEkd5hUWd1O5ZwZCWRrbNkuI/edit?usp=sharing)

#### Software Security Lead

The defined persona for [somebody responsible for a customer's application security](/handbook/customer-success/tam/engagement#software-security-lead).

- [TAM role](https://docs.google.com/document/d/1SIgs3mDwmrotLl78WONF9SQhTzeFi9s8vhpUP0k3kzk/edit?usp=sharing)
- [Customer role](https://docs.google.com/document/d/1LAIIj-0HSXlxdhUWMdp78IcVqAIL88EwQwTd9BAoumw/edit?usp=sharing)
- [Post-scenario review](https://docs.google.com/document/d/1xUVhvuFTcS8v-jd6NN6IHAjg7Cd-caPFsci7eHfj4yY/edit?usp=sharing)
